var express = require('express');
var router = express.Router();



router
.get('/about', function(req, res) {
    res.render('about', { title: 'About' });
})

.get('/product', function(req, res) {
    res.render('product', { title: 'Product' });
})

.get('/join', function(req, res) {
    res.render('join', { title: 'Join' });
});






// .get('/sucesslogin',user.checkoutLogin)    //確認是否有登入，沒有登入不可讀取接下來頁面
// .get('/sucesslogin',controllers.sucesslogin) //登入成功測試，並且轉頁

// .get('/finalpage',user.checkoutLogin)    //確認是否有登入，沒有登入不可讀取接下來頁面
// .get('/finalpage',controllers.Tofinalpage)   //登出畫面

// .get('/logout',user.checkoutLogin)       //確認是否有登入，沒有登入不可讀取接下來頁面
// .get('/logout',user.logout)          //登出邏輯

// .post('/signUp',user.checkoutNotLogin)     //確認是否有登入過，已經登入過則使其失效，避免重複登入的問題
// // .post('/signUp',user.create)        //註冊

// .get('/forgot',               //忘記密碼
//     // user.checkoutLogin, 
//     user.passwordForgot 
//     )

// .post('/forgot',              //送出信箱地址
//     // user.checkoutLogin, 
//     user.handlePasswordForgot 
//     )

// .get('/reset/:token',             //接收從email發出的url，已更改地址
//     // user.checkoutLogin, 
//     user.resetPasswordCheckToken
//     )
// .post('/reset/:token',            //送出修改後密碼
//     // user.checkoutLogin, 
//     user.checkResetPassword
//     )

// .get('/listBerevageData',contro_personalBeverage.list_personalBeverage)  //列出個人飲料記錄

// .get('/listFriendsBerevageData',contro_personalBeverage.list_friendsPersonalBeverage)  //列出朋友飲料記錄

// // ----------------developing----------------------------------
// // .get('/test_created',contro_brands_Restful.test_created)  

// // .get('/test_updated_product',contro_brands_Restful.test_updated_product) 

// // .get('/wantedAndUpdate',contro_brands_Restful.wantedAndUpdate) 
// // ------------------------------------------------------------

// .get('/fetchBeverageBrandsList',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_items_registList.listBrands)  //列出所有品牌

// // .get('/listBrandPos/:the_brand',
// //   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
// //   contro_brands_Restful.listBrandPos)  //列出所有品牌

// .get('/listBrandItem/:the_brand',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_items_registList.listBrandItems)   //列出所有品牌

// // .get('/listBrandRecommendStorePos/:the_brand',
// //   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
// //   contro_brands_Restful.listBrandRecommendStorePos)  //列出所有品牌

// .get('/fetchSpecificItemsList',contro_brands_Restful.listMenu)   //列出該品牌的menu

// .post('/createBerevageData',
//     contro_personalBeverage.validator_reqBody,
//     contro_personalBeverage.validator_beverageItem,
//     contro_personalBeverage.create_personalBeverage)   //增加飲料品項

// .post('/updateBerevageData',
//     contro_personalBeverage.validator_reqBody,
//     contro_personalBeverage.validator_beverageItem,
//     contro_personalBeverage.updateItem_in_list_personalBeverage)   //增加飲料品項

// .post('/deleteItemsOnList',contro_personalBeverage.destoryItem_in_list_personalBeverage) //刪除個人飲料紀錄

// .get('/personalAnalysis',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_personalAnalysis.BITL_list,
//   contro_personalAnalysis.personalAnalysisPage_ET //data 分析
//   )

// .get('/punchcard',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_personalAnalysis.BITL_list,
//   contro_personalAnalysis.personalAnalysisPage_punchcard  //data 分析
//   )    

// .get('/piechart',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_personalAnalysis.BITL_list,
//   contro_personalAnalysis.personalAnalysisPage_piechart //data 分析
//   )    
// .get('/leaderboard',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_personalAnalysis.BITL_list,
//   contro_personalAnalysis.personalAnalysisPage_leaderboard  //data 分析
//   ) 
// .get('/myfriendsBeverageItem',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_items_registList.myfriendsBeverageItem
//   )

// .get('/friends',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.friendPage
//   )
// .get('/friends/search',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.findFriend
//   )
// .post('/friends/queryFriendData',
//   // .get('/friends/queryFriendData',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.confirmFriendRelation,
//   contro_friends.queryFriendData
//   )
// .get('/friends/listLocalFriends',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.transactionPendingHelper,
//   contro_friends.deleteFriendHelper,
//   contro_friends.list_LocalFriends
//   )
// .get('/friends/listInvitedFriends',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.transactionPreinitialCleaner,
//   contro_friends.transactionPendingHelper,
//   contro_friends.cancelInvitationHelper,
//   contro_friends.rejectInvitationHelper,
//   contro_friends.deleteFriendHelper,
//   contro_friends.list_InvitedFriends
//   )
// .get('/friends/listRequestFriends',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.transactionPreinitialCleaner,
//   contro_friends.transactionPendingHelper,
//   contro_friends.cancelInvitationHelper,
//   contro_friends.rejectInvitationHelper,
//   contro_friends.deleteFriendHelper,
//   contro_friends.list_RequestFriends
//   )
// .get('/friends/listFacebookFriends',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.list_FacebookFriends
//   )
// .get('/friends/listGoogleFriends',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.list_GoogleFriends
//   )
// .post('/friends/submitInvitation',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.confirmFriendRelation,
//   contro_friends.transactionPreinitialCleaner,
//   contro_friends.transactionPendingHelper,
//   contro_friends.submit_Invitation,
//   contro_friends.check_DualInvited
//   )
// .post('/friends/confirmInvitation',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.confirm_Invitation
//   )
// .post('/friends/deleteFriend',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.deleteFriend
//   )
// .post('/friends/cancelInvitation',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.cancelInvitation
//   )
// .post('/friends/rejectInvitation',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_friends.rejectInvitation
//   )
// .get('/cooperate/drinkMap',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   cooperation.cooperate_drinkMap
//   )
// .post('/friendsDiscussion/send_message',
//   contro_comments.createMessage
//   )

// // -------------開後台直接看brand and store------------------------------------
// .get('/unitbrands/show',
//   contro_brands_Restful.showUnitbrands
//   )
// // .get('/unitstores/show',
// //  contro_stores_Restful.showUnitstores
// //  )

// // ------------------------------------------------------------
// // -------------Tag--------------------------------------------
// .get('/getTag',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_tag.queryTag
//   )

// .get('/storeTag',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   contro_tag.storeTag
//   )

// // ------------------------------------------------------------


// .get('/setting/profile',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.profile
//   )

// .post('/profile/upload/image',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.uploadProfileImage
//   )

// .post('/profile/delete/image',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.deleteImage
//   )
// .post('/profile/changeprofileimg/image',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.changeProfileImg
//   )
// .post('/profile/cancelprofileimg/image',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.cancelProfileImg
//   )
// .post('/profile/useThirdPartprofileimg/image',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.useThirdPartProfileImg
//   )

// .get('/profile/checkGender',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.checkGender
//   )

// .post('/profile/defaultSetting',
//   user.checkoutLogin,               //確認是否有登入，沒有登入不可讀取接下來頁面
//   user.defaultSetting
//   )

// /// Show files
// .get('/profile/uploads/thumbs/:file',
//   user.checkoutLogin, 
//   user.uploadsthumbs
//   )

// .get('/profile/userData',
//   user.checkoutLogin, 
//   user.userData
//   )

// .get('/myImages',
//   user.checkoutLogin, 
//   user.myImages
//   )

// .get('/verifyEMail/:token',
//   user.verifyEMail
//   )

// .get('/checkVerifyEMail',
//   user.checkoutLogin, 
//   user.checkVerifyEMail
//   );

module.exports = router;