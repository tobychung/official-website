module.exports = function(grunt) {
  	grunt.initConfig({
    
		pkg: grunt.file.readJSON('package.json'),

		
	    sass: {
			dist: {
				files: [{
                    expand: true,
                    cwd: 'public/scss/',      // Src matches are relative to this path.
                    src: ['style_2.scss'], // Actual pattern(s) to match.
                    dest: 'public/css/',
                    ext: '.css',
                    extDot: 'first'   // Extensions in filenames begin after the first dot
                // "testGrunt/*.css": "testGrunt/*.less"
                }]
			}
		},

	  	compass: {
			dist: {
				options: {
					sassDir: 'public/sass',
					cssDir: 'public/css'
				}
			}
		},
		



	    // grunt watch (or simply grunt)
	    watch: {
		    html: {
		    	files: ['**/*.html']
		    },  
		    sass: {
		        files: 'public/scss/*.scss',
		        tasks: ['sass']
		    },
		    options: {
		        livereload: true,
	        }
	    }




    
    });


  	// load plugins

  	// grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');


	//task
	grunt.registerTask('default', ['watch','sass']);
	grunt.registerTask('sass', ['sass']);





};