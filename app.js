var express = require('express');
var path = require('path')
var app = express();

app.set('env','release_official');
// app.set('env','development_aws');
// app.set('env','development_local');
// app.set('env','development_heroku');

if('development_local' == app.get('env')){
  var host = '127.0.0.1';
  var port = 3000;
}else if('development_heroku' == app.get('env')){
  var port = process.env.PORT;
}else if('development_aws' == app.get('env')){
  var port = process.env.PORT || 8060;
}else if('release_official' == app.get('env')){

  var host = '192.168.10.32';
  var port = process.env.PORT || 8080;
}

var monument = require('./controllers/monument');
var bodyParser = require('body-parser');


var about = require('./routes/about'),
	product = require('./routes/product'),
	join = require('./routes/join');



app.set('port',process.env.PROT || port);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));





app.get('/',monument.index);

app.get('/about', about);
app.get('/product', product);
app.get('/join', join);



app.post('/punchTime',monument.punchTime);
app.get('/getPunchTime',monument.getPunchTime);
app.listen(port, host);

console.log("Express server listening on port %d",port);