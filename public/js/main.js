$(document).ready(function() {
	

	var $window = $(window),
        body = $('body');



    //回到top

    if(body.is('#body-index')){

        // console.log("首頁");

        if ($('#back-to-top').length) {
            
            var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
            backToTop();
            $window.on('scroll', function () {
                backToTop();
            });
            $('#back-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });

        }


    }else{}


    



	// 導航列

	$window.on('scroll',function(){
		
		if($(this).scrollTop() >= 50){

			$(".inner-top-navi").css('backgroundColor','#309cd1');
            $("#body-index").find(".logo").css({  opacity:1 , pointerEvents:'default'   });


		}else{
			
			$("#body-index").find(".inner-top-navi").css('backgroundColor','rgba(27,29,44,0)');
            $("#body-index").find(".logo").css({  opacity:0 , pointerEvents:'none'   });
		}
	});
	


	//導航漢堡

	  var hamburger = $('#hamburger-icon');
	  hamburger.click(function() {


        var ul = $('.menu-mobile');

        if( hamburger.hasClass('active')){


            ul.css({ opacity: 0, pointerEvents: 'none' });



        }else{

            ul.css({ opacity: 1, pointerEvents: 'initial' });


        }
        






	     hamburger.toggleClass('active');
	     return false;
	  });
	

    //影子特效 


    $(".effect-ming").on('mouseenter',function (e) {

        var target = $( e.target );

        target.find(".shadow-blur").css("opacity","1");
    
    });

    $(".effect-ming").on('mouseleave',function (e) {

        var target = $( e.target );

        target.find(".shadow-blur").css("opacity","0");
    
    });


    //RWD
    // $(window).resize(function() {

    //     var w = $(window).width();

    //     if(w > 1185){

    //         var margin = $(window).width() * 0.6;
    //         console.log($(window).width());
    //         $(".textbox_left").css("marginRight",margin+ "px");


    //     }else{}
        



    // });



    //


    //手機版
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        

        $('.footer').find('p').replaceWith( "<p>© 2016 CloudTop International Inc.</p>" );


    }





	$window.load(function() {
		whresize();
		wwresize();
	});
	$window.resize(function() {
		whresize();
		wwresize();
	});




	function whresize(){
		var wh = $(window).height();
		$("#top").height(wh*0.9);
		$(".slogan").height(wh*0.9);
		$(".menu_m").height(wh+30);
	}

	function wwresize(){
		var ww = $(window).width();
		if(ww <= 768){
			$("#header_web").css('display', 'none');
			$("#header_mob").css('display', 'block');
		}else{
			$("#header_mob").css('display', 'none');
			$("#header_web").css('display', 'block');
		};
	};

	
		$(".slogan").addClass('wow').addClass('animated').addClass('fadeInDown');

		$(".slogin_second_tit_bg").addClass('wow').addClass('animated').addClass('zoomIn');
		$(".slogin_second_con_bg").addClass('wow').addClass('animated').addClass('zoomIn');

		$(".about_info .title , .service_in .title , .join_in .title").addClass('wow').addClass('animated').addClass('zoomIn');
		$(".about_info .info , .join_in .info").addClass('wow').addClass('animated').addClass('fadeInLeft');
		$(".about_info .btn_link , .service_in .btn_link , .join_in .btn_link").addClass('wow').addClass('animated').addClass('fadeIn');
		

		$(".service_in .info").addClass('wow').addClass('animated').addClass('fadeInRight');






});